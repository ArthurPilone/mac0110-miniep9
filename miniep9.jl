#__________________________________________________________________
#MAC0110 - MiniEP 9 - 6/2020
#Por Lorenzo Bertin [11795356] e Arthur Pilone [11795450]
#-------------------------------------------------------------------

#------------------------------Parte 1-------------------------------

#    Função multiplica, fortemente inspirada na função de mesmo nome
# desenvolvida durante aula, que será usada para esta parte do EP:

function multiplica(matrixA, matrixB)
    dimsA = size(matrixA)
    dimsB = size(matrixB)

    if dimsA[2] != dimsB[1]
        return -1
    end

    matrixResultado = zeros(dimsA[1],dimsB[2])

    for i in 1:dimsA[1]
        for j in 1:dimsB[2]
            for k in 1:dimsA[2]
                matrixResultado[i,j] = matrixResultado[i,j] +  (matrixA[i,k]  *  matrixB[k,j])
            end
        end
    end

    return matrixResultado
end


function matrix_pot(matrix, pot)
    produto = matrix

    pot -= 1
    while pot > 0
        produto = multiplica(produto, matrix)
        pot-=1
    end

    return produto
end

# Testes p/ Parte 1
using Test

function testarParte1()
    @test matrix_pot([1 2; 3 4],1) == [1 2; 3 4]
    @test matrix_pot([1 2 3; 4 5 6; 7 8 9],1) == [1 2 3; 4 5 6; 7 8 9]
    @test matrix_pot([1 0; 0 1],1) == [1 0; 0 1]
    @test matrix_pot([1 2; 3 4],2) == [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot([1 2 3; 0 -1 -2; 1 4 1],3) == [6.0 4.0 -10.0; -2.0 3.0 8.0; -2.0 -14.0 -2.0]
    println("Fim dos testes")
end
# testarParte1()

#------------------------------Parte 2-------------------------------

function matrix_pot_by_squaring(M,p)
    if p == 1
        return M
    elseif p % 2 == 0
        p = p / 2
        return matrix_pot_by_squaring(multiplica(M,M),p)
    elseif p % 2 != 0
        p = (p - 1)/2
        return multiplica(M,matrix_pot_by_squaring(multiplica(M,M),p))
    end
end 


function testarParte2()
    @test matrix_pot_by_squaring([1 2; 3 4],1) == [1 2; 3 4]
    @test matrix_pot_by_squaring([1 2 3; 4 5 6; 7 8 9],1) == [1 2 3; 4 5 6; 7 8 9]
    @test matrix_pot_by_squaring([1 0; 0 1],1) == [1 0; 0 1]
    @test matrix_pot_by_squaring([1 2; 3 4],2) == [7.0 10.0 ; 15.0 22.0]
    @test matrix_pot_by_squaring([1 2 3; 0 -1 -2; 1 4 1],3) == [6.0 4.0 -10.0; -2.0 3.0 8.0; -2.0 -14.0 -2.0]
    @test matrix_pot_by_squaring([1 2; 3 4], 25) == [4.287013497383276e17 6.248009682403692e17; 9.372014523605537e17 1365902802098881294]
    println("Fim dos testes")
end
# testarParte2()

#------------------------------Parte 3-------------------------------

using LinearAlgebra

function compare_times1()
    M = Matrix(LinearAlgebra.I, 30, 30)
    @time matrix_pot(M, 500)
    @time matrix_pot_by_squaring(M, 500)
end
# compare_times1()
function compare_times2()
    M = Matrix(LinearAlgebra.I, 50, 50)
    @time matrix_pot(M, 5000)
    @time matrix_pot_by_squaring(M, 5000)
end
# compare_times2()